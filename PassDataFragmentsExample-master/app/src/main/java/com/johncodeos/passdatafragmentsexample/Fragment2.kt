package com.johncodeos.passdatafragmentsexample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.johncodeos.passdatafragmentsexample.databinding.Fragment2Binding
import com.squareup.picasso.Picasso

class Fragment2 : Fragment(R.layout.fragment_2) {

    private var inputText: String? = ""

    private var fragment2Binding: Fragment2Binding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = Fragment2Binding.inflate(inflater, container, false)
        fragment2Binding = binding
        inputText = arguments?.getString("input_txt")

        val arr = inputText?.split("@")?.toTypedArray()
        binding.outputTextview.text = "Shown: " + arr?.get(1) + " times"
        binding.outputTextview2.text = "Name: " + arr?.get(0)
        binding.outputTextview3.text = "Year: " + arr?.get(2)

        Picasso.get().load(arr?.get(3)).into(binding.outputTextview4);
        binding.outputTextview5.text = "Number of Seasons: " + arr?.get(4)

        return binding.root
    }

    override fun onDestroyView() {
        fragment2Binding = null
        super.onDestroyView()
    }

}