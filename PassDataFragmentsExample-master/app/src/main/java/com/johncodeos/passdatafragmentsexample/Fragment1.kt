package com.johncodeos.passdatafragmentsexample

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.johncodeos.passdatafragmentsexample.databinding.Fragment1Binding
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Fragment1 : Fragment(R.layout.fragment_1) {

    private lateinit var comm: Communicator
    private val movieList = ArrayList<MovieModel>()
    private lateinit var moviesAdapter: MoviesAdapter

    private var fragment1Binding: Fragment1Binding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = Fragment1Binding.inflate(inflater, container, false)
        fragment1Binding = binding



        val recyclerView: RecyclerView = binding.recyclerView
        moviesAdapter = MoviesAdapter(movieList,::OnItemClick)
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = moviesAdapter
        prepareMovieData()

        comm = requireActivity() as Communicator
        binding.button.setOnClickListener {
            comm.passDataCom2("test".toString())
        }



        return binding.root
    }

    override fun onDestroyView() {
        fragment1Binding = null
        super.onDestroyView()
    }

    fun OnItemClick (movieModel: MovieModel){


        comm = requireActivity() as Communicator

        comm.passDataCom(movieModel.getTitle().toString()+"@"+movieModel.getGenre().toString()+"@"+movieModel.getYear().toString()+"@"+movieModel.getImage().toString()+"@"+movieModel.getS().toString())


    }
    private fun prepareMovieData() {
        var movie = MovieModel("Mad Max: Fury Road", "123", "2015", image= "https://raw.githubusercontent.com/mitchtabian/Blog-Images/master/digital_ocean.png", s="23")
        movieList.add(movie)
        movie = MovieModel("Inside Out", "456", "2015" ,image= "https://pngimg.com/uploads/mario/mario_PNG53.png", s="2")
        movieList.add(movie)
        movie = MovieModel("Star Wars: Episode VII - The Force Awakens", "213", "2015" ,image= "https://pngimg.com/uploads/mario/mario_PNG53.png", s="12")
        movieList.add(movie)
        movie = MovieModel("Shaun the Sheep", "54366", "2015" ,image= "https://pngimg.com/uploads/mario/mario_PNG53.png", s="3")
        movieList.add(movie)
        movie = MovieModel("The Martian", "324234", "2015", image= "https://pngimg.com/uploads/mario/mario_PNG53.png", s="53")
        movieList.add(movie)
        movie = MovieModel("Mission: Impossible Rogue Nation", "456456", "2015" ,image= "https://pngimg.com/uploads/mario/mario_PNG53.png", s="332")
        movieList.add(movie)
        movie = MovieModel("Up", "5464564", "2009" , image= "https://pngimg.com/uploads/mario/mario_PNG53.png", s="213")
        movieList.add(movie)

        moviesAdapter.notifyDataSetChanged()
    }
}