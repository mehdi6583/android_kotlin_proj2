package com.johncodeos.passdatafragmentsexample
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.squareup.picasso.Picasso

internal class MoviesAdapter(private var moviesList: List<MovieModel>,  private val listener: (MovieModel) -> Unit ) :
    RecyclerView.Adapter<MoviesAdapter.MyViewHolder>() {
    internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView = view.findViewById(R.id.title)
        var year: TextView = view.findViewById(R.id.year)
        var genre: TextView = view.findViewById(R.id.genre)
        var image : ImageView =  view.findViewById(R.id.image)
    }
    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = moviesList[position]
        holder.title.text = movie.getTitle()
        holder.genre.text = "shown : "+ movie.getGenre() + "times"
        holder.year.text = movie.getYear()
        val requestOptions = RequestOptions()
            .placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background)
        Glide.with(holder.itemView.context).applyDefaultRequestOptions(requestOptions).load(movie.getImage()).into(holder.image)

//        Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(holder.image);
        holder.itemView.setOnClickListener { listener(moviesList[position]) }
    }
    override fun getItemCount(): Int {
        return moviesList.size
    }
}