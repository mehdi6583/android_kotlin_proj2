package com.johncodeos.passdatafragmentsexample
class MovieModel(title: String?, genre: String?, year: String? , image: String? , s : String?) {
    private var title: String
    private var genre: String
    private var year: String
    private var image: String
    private var s : String
    init {
        this.title = title!!
        this.genre = genre!!
        this.year = year!!
        this.image = image!!
        this.s = s !!

    }
    fun getTitle(): String? {
        return title
    }
    fun setTitle(name: String?) {
        title = name!!
    }
    fun getYear(): String? {
        return year
    }
    fun setYear(year: String?) {
        this.year = year!!
    }
    fun getGenre(): String? {
        return genre
    }
    fun setGenre(genre: String?) {
        this.genre = genre!!
    }

    fun getImage(): String? {
        return image
    }
    fun setImage(image: String?) {
        this.image = image!!

    }

    fun getS(): String? {
        return s
    }
    fun setS(s: String?) {
        this.s = s!!
    }
}